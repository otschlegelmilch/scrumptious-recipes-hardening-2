# Generated by Django 4.0.3 on 2022-09-01 21:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('meal_plans', '0003_remove_mealplans_owner_mealplans_user'),
    ]

    operations = [
        migrations.RenameField(
            model_name='mealplans',
            old_name='user',
            new_name='owner',
        ),
    ]
