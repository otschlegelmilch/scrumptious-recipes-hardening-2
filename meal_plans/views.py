from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from meal_plans.models import MealPlans
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect

from recipes.forms import CalendarForm

# Create your views here.


class MealPlansListView(ListView):
    model = MealPlans
    template_name = "meal_plans/list.html"
    paginate_by = 4

    def get_queryset(self):
        return MealPlans.objects.filter(user=self.request.user)


class MealPlansCreateView(CreateView):
    model = MealPlans
    template_name = "meal_plans/create.html"
    success_url = reverse_lazy("meal_plans")
    form_class = CalendarForm

    # def form_valid(self, form):
    #     form.instance.user = self.request.user
    #     return super().form_valid(form)
    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.user = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)


class MealPlansDetailView(LoginRequiredMixin, DetailView):
    model = MealPlans
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlans.objects.filter(user=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlans
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans")


class MealPlansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlans
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans")
