from django.urls import path

from meal_plans.views import (
    MealPlansListView,
    MealPlansCreateView,
    MealPlansDetailView,
    MealPlansUpdateView,
    MealPlansDeleteView,
)

urlpatterns = [
    path("", MealPlansListView.as_view(), name="meal_plans"),
    path(
        "create/",
        MealPlansCreateView.as_view(),
        name="meal_plans_create",
    ),
    path(
        "<int:pk>/",
        MealPlansDetailView.as_view(),
        name="meal_plans_detail",
    ),
    path(
        "<int:pk>/edit/",
        MealPlansUpdateView.as_view(),
        name="meal_plans_update",
    ),
    path(
        "<int:pk>/delete/",
        MealPlansDeleteView.as_view(),
        name="meal_plans_delete",
    ),
]
