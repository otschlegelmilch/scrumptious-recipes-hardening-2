from django.db import models
from django.conf import settings

from recipes.models import Recipe
from django.contrib.auth.mixins import LoginRequiredMixin

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class MealPlans(LoginRequiredMixin, models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    user = models.ForeignKey(
        USER_MODEL, on_delete=models.CASCADE, null=True, editable=False
    )
    # user = models.ForeignKey(
    #     USER_MODEL, related_name="user", on_delete=models.CASCADE
    # )
    recipes = models.ManyToManyField(Recipe, related_name="meal_plans")
