from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from recipes.forms import RatingForm
from .models import Ingredient
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods

# from recipes.forms import RecipeForm
from recipes.models import Recipe, ShoppingItems

# I know having the comments in the code makes it look like
# I copy/pasted from the learn, but I actually just typed it in
# there for notation


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        # Create a new empty list and assign it to a variable
        added_shopping_items = []

        # For each item in the user's shopping items, which we
        # can access through the code self.request.user.shopping_items.all
        for shopping_item in self.request.user.shopping_items.all():
            added_shopping_items.append(shopping_item)

        # Put that list into the context
        context["food_in_shopping_list"] = added_shopping_items
        return context


# all need a login.mixin
class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = [
        "name",
        "description",
        "image",
    ]
    success_url = reverse_lazy("recipes_list")
    login_url = "/login/"
    redirect_field_name = "recipe_list"
    LOGIN_REDIRECT_URL = "recipes_list"
    LOGOUT_REDIRECT_URL = "recipes_list"

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = [
        "name",
        "author",
        "description",
        "image",
    ]
    success_url = reverse_lazy("recipes_list")
    login_url = "/login/"
    redirect_field_name = "recipe_list"
    LOGIN_REDIRECT_URL = "recipes_list"
    LOGOUT_REDIRECT_URL = "recipes_list"


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")
    login_url = "/login/"
    redirect_field_name = "recipes_list"
    LOGIN_REDIRECT_URL = "recipes_list"
    LOGOUT_REDIRECT_URL = "recipes_list"


# This view will show a list of shopping items that was created by the user.
class ShoppingItemsListView(ListView):
    model = ShoppingItems
    template_name = "shopping_items/list.html"


class ShoppingItemsCreateView(CreateView):
    model = ShoppingItems
    template_name = "shopping_items/create.html"
    fields = ["user", "food_item"]

    def get_queryset(self):
        return RecipeDetailView.objects.filer().get_queryset()


@require_http_methods(["POST"])
def delete_all_shopping_items(request):
    # Delete all of the shopping items for the user
    ShoppingItems.objects.filter(user=request.user).delete()

    return redirect("shopping_items_list")


# class CreateShoppingItem(Model):


@require_http_methods(["POST"])
def create_shopping_item(request):
    # Get the ingredient_id from the POST
    ingredient_id = request.POST.get("ingredient_id")
    # Get the specific ingredient from the Ingredient model
    ingredient = Ingredient.objects.get(id=ingredient_id)
    # Get the current user
    user = request.user

    # class Meta:
    #     unique_together = (("key1", "key2"),)

    # key1 = models.IntegerField(primary_key=True)
    # key2 = models.IntegerField()

    try:
        # Create the new shopping item in the database
        ShoppingItems.objects.create(
            food_item=ingredient.food,
            user=user,
        )
    # Catch the error if its already in there
    except IntegrityError:
        pass
    # Go back to the recipe page
    return redirect("recipe_detail", pk=ingredient.recipe.id)
