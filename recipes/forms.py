from django import forms
from meal_plans.models import MealPlans
from django.forms import ModelForm

from recipes.models import Recipe
from recipes.models import Rating


class RecipeForm(forms.ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "name",
            "author",
            "description",
            "image",
        ]


class RatingForm(forms.ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]


class DateInput(forms.DateInput):
    input_type = "date"


class CalendarForm(ModelForm):
    class Meta:
        model = MealPlans
        fields = ["name", "date", "recipes"]
        widgets = {
            "date": DateInput(),
        }


class ShoppingForm(forms.ModelForm):
    pass
